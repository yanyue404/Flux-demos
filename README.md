# Flux-demos
> 学习Flux架构

## Guide

- [Flux 架构入门教程](http://www.ruanyifeng.com/blog/2016/01/flux.html) by ruanyifeng
- [rails365.net](https://www.rails365.net/playlists)

### redux

- [官方文档](https://redux.js.org/) / [中文文档](http://cn.redux.js.org/)
- [Redux入门教程](http://www.ruanyifeng.com/blog/2016/09/redux_tutorial_part_one_basic_usages.html) , by [ruanyifeng](https://github.com/ruanyf)
- [Redux Tutorial 中文翻译](https://github.com/react-guide/redux-tutorial-cn) ，by [React Guide](https://github.com/react-guide)

### react-redux

- [官方文档](https://redux.js.org/basics/usage-with-react)  / [中文文档]()

## Useful links
- https://github.com/reduxjs/react-redux/blob/master/docs/api.md
- https://www.jianshu.com/p/06f5285e2620







